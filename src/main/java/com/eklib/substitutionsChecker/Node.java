package com.eklib.substitutionsChecker;

import java.util.ArrayList;
import java.util.List;

public class Node {

    private String state;
    private List<Node> nodes;

    public Node(String state) {
        this.state = state;
    }

    public List<Node> getNodes() {
        return nodes;
    }

    public void setNodes(List<Node> nodes) {
        this.nodes = nodes;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void addNode(Node node) {
        if (nodes == null)
            nodes = new ArrayList<>();
        nodes.add(node);
    }
}
