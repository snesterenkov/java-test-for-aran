package com.eklib.substitutionsChecker;

import org.apache.commons.collections4.bidimap.DualHashBidiMap;

import java.util.HashSet;
import java.util.Set;

public class SubstitutionsService {

    private static final String EMPTY_STRING = "";

    private String expectedResult;
    private Set<String> uniqueStates;
    private DualHashBidiMap<String, String> substitutionsMap;

    public SubstitutionsService(DualHashBidiMap<String, String> substitutionsMap, String expectedResult) {
        this.substitutionsMap = substitutionsMap;
        this.uniqueStates = new HashSet<>();
        this.expectedResult = expectedResult;
    }

    public boolean nodeContainExpectedResult(Node parent) {

        String currentState = parent.getState();
        uniqueStates.add(currentState);
        if (uniqueStates.contains(expectedResult))
            return true;

        int position = 0;
        while (position < currentState.length()) {
            String foundCombination = findCombination(currentState, EMPTY_STRING, position++);
            if (!foundCombination.equals(EMPTY_STRING)) {
                String childState = "";
                Set<Node> children = new HashSet<>();
                if (currentState.equals(foundCombination)) {
                    childState = substitutionsMap.get(foundCombination);
                    if (!uniqueStates.contains(childState))
                        children.add(new Node(childState));
                } else {
                    if (parent.getNodes() != null) {
                        for (Node node : parent.getNodes()) {
                            childState = node.getState().replaceAll(foundCombination, substitutionsMap.get(foundCombination));
                            if (!uniqueStates.contains(childState)) {
                                children.add(new Node(childState));
                                uniqueStates.add(childState);
                            }
                        }
                    }
                    childState = currentState.replaceAll(foundCombination, substitutionsMap.get(foundCombination));
                    if (!uniqueStates.contains(childState)) {
                        children.add(new Node(childState));
                        uniqueStates.add(childState);
                    }
                }
                children.forEach(parent::addNode);
            }
            if (!foundCombination.equals(EMPTY_STRING)) {
                uniqueStates.add(foundCombination);
            }
        }
        if (parent.getNodes() != null) {
            for (Node child : parent.getNodes()) {
                if (nodeContainExpectedResult(child))
                    return true;
            }
        }
        return false;
    }


    private String findCombination(String state, String currentCombination, int startPosition) {
        currentCombination += state.substring(startPosition, ++startPosition);
        if (!substitutionsMap.keySet().contains(currentCombination)) {
            if (startPosition >= state.length())
                return EMPTY_STRING;
            return findCombination(state, currentCombination, startPosition);
        } else {
            return currentCombination;
        }
    }

}
