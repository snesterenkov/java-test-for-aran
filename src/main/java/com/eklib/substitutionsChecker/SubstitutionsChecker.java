package com.eklib.substitutionsChecker;

import org.apache.commons.collections4.bidimap.DualHashBidiMap;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SubstitutionsChecker {


    private static final String SUBSTITUTION_PATTERN = "\\w+->(?:\\w+|)";
    private static final String ARROW = "->";
    private static final String EMPTY_STRING = "";


    public boolean canReachEnd(String start, String end, String[] substitutions) {
        /*
         * returns true if the substitutions can get from the start letter to the end letter, with no leftover letters.
         * The substitutions can be used in any order, i.e. they do not have to be applied in the order defined by the array.
         *
         * returns false if there is no way for the substitutions to get from the start letter to the end letter, with no leftover letters.
         *
         * Feel free to use 3rd party dependencies from Maven etc. to help with your solution.
         *
         */
        if (isValidSubstitutes(substitutions)) {
            DualHashBidiMap<String, String> substitutionsMap = castToMap(substitutions);
            if (substitutionsMap.containsKey(start) && substitutionsMap.containsValue(end)) {
                SubstitutionsService substitutionsService = new SubstitutionsService(substitutionsMap, end);
                return substitutionsService.nodeContainExpectedResult(new Node(substitutionsMap.get(start)));
            }
        }
        return false;
    }

    private DualHashBidiMap<String, String> castToMap(String[] substitutions) {
        DualHashBidiMap<String, String> substitutionsMap = new DualHashBidiMap<>();
        for (String substitution : substitutions) {
            String[] substitutionParts = substitution.split(ARROW);
            substitutionsMap.put(substitutionParts[0], substitutionParts.length > 1 ? substitutionParts[1] : EMPTY_STRING);
        }
        return substitutionsMap;
    }

    private boolean isValidSubstitutes(String[] substitutions) {
        Pattern p = Pattern.compile(SUBSTITUTION_PATTERN);
        for (String substitution : substitutions) {
            Matcher matcher = p.matcher(substitution);
            if (!matcher.matches())
                return false;
        }
        return true;
    }

}