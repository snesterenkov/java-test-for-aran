package com.eklib.substitutionsChecker;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class SubstitutionsCheckerTest {

    private SubstitutionsChecker substitutionsChecker = new SubstitutionsChecker();

    @Test
    public void canReachEnd_returns_true_when_matching_end_letter_and_no_residue_letters() {
        String start = "A";
        String end = "I";
        String[] substitutions = {"A->BC", "BC->D", "D->EF", "E->G", "F->H", "GH->I"};
        boolean result = substitutionsChecker.canReachEnd(start, end, substitutions);
        assertTrue(result);
    }

    @Test
    public void canReachEnd_returns_true_when_matching_end_letter_and_no_residue_letters_with_blank() {
        String start = "A";
        String end = "I";
        String[] substitutions = {"A->BC", "BC->D", "D->EF", "E->G", "F->H", "GH->LK", "L->", "K->I" };
        boolean result = substitutionsChecker.canReachEnd(start, end, substitutions);
        assertTrue(result);
    }

    @Test
    public void canReachEnd_returns_false_when_no_matching_end_letter_and_no_residue_letters(){
        String start = "A";
        String end = "I";
        String[] substitutions = {"A->BC", "BC->D", "D->EF", "E->G", "F->H", "GH->J"};
        boolean result = substitutionsChecker.canReachEnd(start, end, substitutions);
        assertFalse(result);
    }

    @Test
    public void  canReachEnd_returns_false_when_residue_letters(){
        String start = "A";
        String end = "I";
        String[] substitutions = {"A->BC", "BC->D", "D->EF", "E->G", "F->H", "GH->IJ"};
        boolean result = substitutionsChecker.canReachEnd(start, end, substitutions);
        assertFalse(result);
    }

}